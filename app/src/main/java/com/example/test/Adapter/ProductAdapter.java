package com.example.test.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.test.Model.ProductModel;
import com.example.test.R;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    private final ArrayList<ProductModel> listProduct;

    public ProductAdapter(ArrayList<ProductModel> listProduct) {
        this.listProduct = listProduct;
    }

    @Override
    public ProductAdapter.ProductViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull  ProductAdapter.ProductViewHolder holder, int position) {
        ProductModel product = listProduct.get(position);
        holder.title.setText(product.getTitle());
        holder.price.setText("$"+product.getPrice());
        holder.desc.setText(product.getDescription());
        holder.category.setText(product.getCategory());

        if (!product.getImage().equals("null")) {
            Glide.with(holder.itemView.getContext())
                    .load(product.getImage())
                    .into(holder.product);
        }
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        TextView title,price,desc,category;
        ImageView product;
        public ProductViewHolder(@NonNull  View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_title);
            price = itemView.findViewById(R.id.tv_price);
            desc = itemView.findViewById(R.id.tv_desc);
            category = itemView.findViewById(R.id.tv_category);
            product = itemView.findViewById(R.id.iv_product);
        }
    }
}
