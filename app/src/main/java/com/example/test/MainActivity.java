package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.et_login_email)
    TextInputEditText email;
    @BindView(R.id.et_login_pass)
    TextInputEditText pass;
    @BindView(R.id.btn_login)
    Button btnLogin;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(MainActivity.this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailL = email.getText().toString().trim();
                String passw = pass.getText().toString().trim();

                boolean isEmpty = false;
                boolean isInvalidLengthPassword = false;

                if (emailL.isEmpty()) {
                    isEmpty = true;
                    email.setError("Required");
                }

                if (passw.isEmpty()) {
                    isEmpty = true;
                    pass.setError("Required");
                }

                if (passw.length() < 6) {
                    isInvalidLengthPassword = true;
                    pass.setError("The password must be at least 6 characters");
                }

                if (!isEmpty && !isInvalidLengthPassword) {
                    progressDialog.setMessage("Logging you in...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    AndroidNetworking.post("https://fakestoreapi.com/auth/login")
                            .addBodyParameter("username", emailL)
                            .addBodyParameter("password", passw)
                            .setPriority(Priority.MEDIUM)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        String token = response.getString("token");
                                        Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                                        intent.putExtra("token", token);
                                        startActivity(intent);

                                        progressDialog.dismiss();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        try {
                                            Toast.makeText(getApplicationContext(), response.getString("msg"), Toast.LENGTH_SHORT).show();
                                        } catch (JSONException jsonException) {
                                            jsonException.printStackTrace();
                                        }
                                        progressDialog.dismiss();
                                    }
                                }

                                @Override
                                public void onError(ANError anError) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), anError.getErrorBody(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });
    }
}